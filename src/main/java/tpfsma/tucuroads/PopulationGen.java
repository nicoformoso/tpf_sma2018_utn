package tpfsma.tucuroads;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.Population;
import org.matsim.api.core.v01.population.PopulationWriter;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.transformations.GeotoolsTransformation;
import org.matsim.core.utils.geometry.transformations.TransformationFactory;

public class PopulationGen implements Runnable {

	/* 
	 * Coordinates
	 * 
	 * This Well Known Text (WKT) corresponds to the coordinates region
	 * where Tucumán is located in.
	 */
	public static final String UTM20S = 
			"PROJCS[\"WGS 84 / UTM zone 20S\",\n" + 
			"    GEOGCS[\"WGS 84\",\n" + 
			"        DATUM[\"WGS_1984\",\n" + 
			"            SPHEROID[\"WGS 84\",6378137,298.257223563,\n" + 
			"                AUTHORITY[\"EPSG\",\"7030\"]],\n" + 
			"            AUTHORITY[\"EPSG\",\"6326\"]],\n" + 
			"        PRIMEM[\"Greenwich\",0,\n" + 
			"            AUTHORITY[\"EPSG\",\"8901\"]],\n" + 
			"        UNIT[\"degree\",0.01745329251994328,\n" + 
			"            AUTHORITY[\"EPSG\",\"9122\"]],\n" + 
			"        AUTHORITY[\"EPSG\",\"4326\"]],\n" + 
			"    UNIT[\"metre\",1,\n" + 
			"        AUTHORITY[\"EPSG\",\"9001\"]],\n" + 
			"    PROJECTION[\"Transverse_Mercator\"],\n" + 
			"    PARAMETER[\"latitude_of_origin\",0],\n" + 
			"    PARAMETER[\"central_meridian\",-63],\n" + 
			"    PARAMETER[\"scale_factor\",0.9996],\n" + 
			"    PARAMETER[\"false_easting\",500000],\n" + 
			"    PARAMETER[\"false_northing\",10000000],\n" + 
			"    AUTHORITY[\"EPSG\",\"32720\"],\n" + 
			"    AXIS[\"Easting\",EAST],\n" + 
			"    AXIS[\"Northing\",NORTH]]";

	private GeotoolsTransformation gtt = new GeotoolsTransformation(TransformationFactory.WGS84, UTM20S);

	/*
	 * Files
	 */
	private static final String configFile  = "./input/config.xml"; 
	private static final String homeEntries = "./input/home_entries.txt";
	private static final String workEntries = "./input/work_entries.txt";
		
	private List<String> homeNames = new ArrayList<String>();
	private List<String> workNames = new ArrayList<String>();
	
	private Map<String, Coord> zoneGeometries = new HashMap<String, Coord>();

	private Network network;
	private Scenario scenario;
	private Population population;
	
	public static void main(String[] args) {
		/* Creating an object of my own */
		PopulationGen popGen = new PopulationGen();
		/* Running me */
		popGen.run();
	}

	public void run() {
		Config configXML = ConfigUtils.loadConfig(configFile);
		
		/*
		 * Scenario
		 *
		 * We need to create a new scenario object, in order to retrieve 
		 * the population object later.
		 *
		 */
		scenario = ScenarioUtils.createScenario(configXML);

		/*
		 * Retrieving population object from scenario
		 */
		population = scenario.getPopulation();
		
		/*
		 * Retrieving network object from scenario
		 */
		network = scenario.getNetwork();

		fillZoneData();
		generatePopulation();
		
		/*
		 * Writing population.xml file 
		 */
		PopulationWriter populationWriter = new PopulationWriter(population, network);
		populationWriter.write("./output/tucuroads/population.xml");
	}

	private void fillZoneData() {
		/*
		 * Reading home entries 
		 */
		try {
			BufferedReader buffReader = new BufferedReader(new FileReader(homeEntries));
			buffReader.readLine(); /* skip file header */
			
			/* file fields id */
			int homeName 	  = 0;
			int homeLatitude  = 1;
			int homeLongitude = 2;
			
			String line;
			while ((line = buffReader.readLine()) != null) {
				String[] lineParts = line.split("\t");
				Coord homeCoords = new Coord(Double.parseDouble(lineParts[homeLatitude]), Double.parseDouble(lineParts[homeLongitude]));
				/* 
				 * Loading home place, and its coordinates transformed into
				 * cartesian values 
				 */
				zoneGeometries.put(lineParts[homeName], gtt.transform(homeCoords) );
				homeNames.add(lineParts[homeName]);
			}

			buffReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		/*
		 * Reading work entries 
		 */
		try {
			BufferedReader buffReader = new BufferedReader(new FileReader(workEntries));
			buffReader.readLine(); /* skip file header */
			
			/* file fields id */
			int workName 	  = 0;
			int workLatitude  = 1;
			int workLongitude = 2;
			
			String line;
			while ((line = buffReader.readLine()) != null) {
				String[] lineParts = line.split("\t");
				Coord workCoords = new Coord(Double.parseDouble(lineParts[workLatitude]), Double.parseDouble(lineParts[workLongitude]));
				/* 
				 * Loading work place, and its coordinates transformed into
				 * cartesian values 
				 */
				zoneGeometries.put(lineParts[workName], gtt.transform(workCoords));
				workNames.add(lineParts[workName]);
			}
			
			buffReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void generatePopulation() {
		/* to-do: implement random number */
		// Random travelNum = new Random();

		for (String homeName : homeNames) {
			for (String workName : workNames) {
				generateHomeWorkHomeTrips(homeName, workName, 200);
			}
		}
	}

	private void generateHomeWorkHomeTrips(String from, String to, int quantity) {
		for (int i=0; i<quantity; ++i) {

			/* Retrieving coordinates */
			Coord source = zoneGeometries.get(from);
			Coord sink = zoneGeometries.get(to);
			
			/* 
			 * Adding a distortion to the coordinates, to get 
			 * a pseudo randomness
			 */
			Coord homeLocation = shoot(source);
			Coord workLocation = shoot(sink);
			
			/* Creating a person */
			Person person = population.getFactory().createPerson(createId(from, to, i, TransportMode.car));
			
			/* Creating a plan */
			Plan plan = population.getFactory().createPlan();
			
			/* Creating activities */
			final Activity homeActivity = createHome(homeLocation);
			final Activity workActivity = createWork(workLocation);

			/* Adding things to the plan */
			plan.addActivity( homeActivity );
			plan.addLeg(createDriveLeg());
			plan.addActivity(workActivity);
			plan.addLeg(createDriveLeg());
			plan.addActivity(createHome(homeLocation));
			
			/* Adding plan to the person */
			person.addPlan(plan);
			
			/* Adding person to the population */
			population.addPerson(person);
		}
	}

	private Activity createWork(Coord workLocation) {
		final int min = 17;
		final int max = 20;
		Random endWorkHour = new Random();
		
		Activity activity = population.getFactory().createActivityFromCoord("work", workLocation);
		activity.setEndTime((endWorkHour.nextInt(max + 1 - min) + min)*60*60);
		return activity;
	}

	private Activity createHome(Coord homeLocation) {
		final int min = 6;
		final int max = 8;
		Random endHomeHour = new Random();
		
		Activity activity = population.getFactory().createActivityFromCoord("home", homeLocation);
		activity.setEndTime((endHomeHour.nextInt(max + 1 - min) + min)*60*60);
		return activity;
	}
	
	private Leg createDriveLeg() {
		/* Creating the route */
		Leg leg = population.getFactory().createLeg(TransportMode.car);
		return leg;
	}

	/*
	 * Adding distortion factor, given by a pseudo-random 
	 * double number: rand.nextDouble()/10
	 * 
	 * The Random generator returns a number within the range [0,1)
	 * therefore, we devide it in 10, to get the distortion
	 */
	private Coord shoot(Coord source) {
		Random rand = new Random();

		double blurred_x = source.getX() + (rand.nextDouble()/10);
		double blurred_y = source.getY() + (rand.nextDouble()/10);

		return new Coord(blurred_x, blurred_y);
	}

	private Id<Person> createId(String source, String sink, int i, String transportMode) {
		return Id.create(transportMode + "_" + source + "_" + sink + "_" + i, Person.class);
	}

}
