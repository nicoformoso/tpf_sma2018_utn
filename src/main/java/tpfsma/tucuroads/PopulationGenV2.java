package tpfsma.tucuroads;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.Population;
import org.matsim.api.core.v01.population.PopulationWriter;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.transformations.GeotoolsTransformation;
import org.matsim.core.utils.geometry.transformations.TransformationFactory;

public class PopulationGenV2 implements Runnable {

	/* 
	 * Coordinates system
	 * 
	 * This Well Known Text (WKT) corresponds to the coordinates region
	 * where Tucumán is located in.
	 */
	public static final String UTM20S = 
			"PROJCS[\"WGS 84 / UTM zone 20S\",\n" + 
			"    GEOGCS[\"WGS 84\",\n" + 
			"        DATUM[\"WGS_1984\",\n" + 
			"            SPHEROID[\"WGS 84\",6378137,298.257223563,\n" + 
			"                AUTHORITY[\"EPSG\",\"7030\"]],\n" + 
			"            AUTHORITY[\"EPSG\",\"6326\"]],\n" + 
			"        PRIMEM[\"Greenwich\",0,\n" + 
			"            AUTHORITY[\"EPSG\",\"8901\"]],\n" + 
			"        UNIT[\"degree\",0.01745329251994328,\n" + 
			"            AUTHORITY[\"EPSG\",\"9122\"]],\n" + 
			"        AUTHORITY[\"EPSG\",\"4326\"]],\n" + 
			"    UNIT[\"metre\",1,\n" + 
			"        AUTHORITY[\"EPSG\",\"9001\"]],\n" + 
			"    PROJECTION[\"Transverse_Mercator\"],\n" + 
			"    PARAMETER[\"latitude_of_origin\",0],\n" + 
			"    PARAMETER[\"central_meridian\",-63],\n" + 
			"    PARAMETER[\"scale_factor\",0.9996],\n" + 
			"    PARAMETER[\"false_easting\",500000],\n" + 
			"    PARAMETER[\"false_northing\",10000000],\n" + 
			"    AUTHORITY[\"EPSG\",\"32720\"],\n" + 
			"    AXIS[\"Easting\",EAST],\n" + 
			"    AXIS[\"Northing\",NORTH]]";

	private GeotoolsTransformation gtt = new GeotoolsTransformation(TransformationFactory.WGS84, UTM20S);

	/*
	 * Files
	 */
	private static final String configurationFile  	= "./input/config.xml"; 
	private static final String homeEntriesFile 	= "./input/home_entries.csv";
	private static final String workEntries 		= "./input/work_entries.csv";
	private static final String workplaceTypesFile 	= "./input/work_types.csv";
	private static final String dummyPointsFile		= "./input/dummy_points.csv";
	
	/*
	 * MATSim required objects 
	 */
	private Network network;
	private Scenario scenario;
	private Population population;
	
	/*
	 * Population variables
	 */

	/* places */
	private List<Coord> homeplaces = new ArrayList<Coord>();
	private Map<String, Float> workplace_types = new HashMap<String, Float>();
	private List<WorkPlace> workplaces = new ArrayList<WorkPlace>();
	private WorkPlaceParser wpp = null;

	/* persons */
	private static final int   totalPersons = 20000;
	private static final float percentWorkers = (float) 0.7;
	private static final float percentTransacting = (float) 0.3;

	public static void main(String[] args) {
		/* Creating an object of my own */
		PopulationGenV2 population = new PopulationGenV2();
		/* Running me */
		population.run();
	}
	
	public void run() {
		Config configXML = ConfigUtils.loadConfig(configurationFile);
		/*
		 * Scenario
		 *
		 * We need to create a new scenario object, in order to retrieve 
		 * the population object later.
		 *
		 */
		scenario = ScenarioUtils.createScenario(configXML);

		/*
		 * Retrieving population object from scenario
		 */
		population = scenario.getPopulation();

		/*
		 * Retrieving network object from scenario
		 */
		network = scenario.getNetwork();

		/* fills 'homeplaces' Map variable */
		fillHomeplaces();

		/* fills 'workplaces' List variable */
		wpp = new WorkPlaceParser(workEntries);
		wpp.fillWorkplacesFromFile();

		/* it isn't needed */
		workplaces = wpp.getAllWorkPlaces();
		
		/* fills 'workplace_types' */
		fillWorkplaceTypes();

		/*
		 * Population generation
		 * 
		 * Each of those methods below, create a population of persons
		 * who works or do transactions. The methods add those persons
		 * into the 'population' object.
		 */

		/* generatig workers */
		generateWorkers();

		/* generate transtacting persons */
		generateTransactPersons();

		/* generating dummy population */
		generateDummyPopulation();
		
		/*
		 * Writing population xml file 
		 */
		PopulationWriter populationWriter = new PopulationWriter(population, network);

		String populationFileName = "population_" + String.valueOf(totalPersons)	+
									"_w_" + String.valueOf(percentWorkers) 		+
									"_t_" + String.valueOf(percentTransacting)	+ ".xml";

		populationWriter.write("./output/tucuroads/" + populationFileName);
	}

	private void generateDummyPopulation() {
		Random rnd = new Random();

		float correction_factor = workplace_types.get("correction");
		float dummyPersonsCount = totalPersons * percentWorkers * correction_factor;
		
		List<String> dummy_city_points_name  = new ArrayList<String>();
		List<Coord> dummy_city_points_coords = new ArrayList<Coord>();
		List<String> dummy_city_points_side  = new ArrayList<String>();
		
		/* reading dummy points from file */
		try {
			FileInputStream fis 	= new FileInputStream(dummyPointsFile);
			InputStreamReader isr 	= new InputStreamReader(fis, Charset.forName("UTF-8"));
			BufferedReader buf_read = new BufferedReader(isr);

			buf_read.readLine(); /* skip header */

			String line;
			while ((line = buf_read.readLine()) != null) {
				String[] variables = line.split("\t");	/* separator = TAB */

				String city_point_name = variables[1];
				Coord city_point_coord = new Coord(
						Double.parseDouble(variables[2]),
						Double.parseDouble(variables[3]));

				dummy_city_points_name.add(city_point_name);
				dummy_city_points_coords.add( gtt.transform(city_point_coord) );
				dummy_city_points_side.add(variables[4]);
			}

			buf_read.close();
			isr.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int dummy_points_size = dummy_city_points_name.size();
		
		/* generating dummy population */
		for ( int i = 0; i < dummyPersonsCount; i++ ) {
			/* getting source point */
			int source_idx = rnd.nextInt(dummy_points_size);
			String source_point_name = dummy_city_points_name.get( source_idx );
			Coord source_point_coord = dummy_city_points_coords.get( source_idx );
			String source_point_side = dummy_city_points_side.get( source_idx );
			
			/* 
			 * getting destination point 
			 * ensuring that it is different from source
			 */
			int destination_idx;
			String destination_point_name;
			Coord destination_point_coord;
			String destination_point_side;
			do {
				destination_idx = rnd.nextInt(dummy_points_size);

				destination_point_name = dummy_city_points_name.get( destination_idx );
				destination_point_coord = dummy_city_points_coords.get( destination_idx );
				destination_point_side = dummy_city_points_side.get( destination_idx );
			} while ( (source_idx != destination_idx) && ( source_point_side != destination_point_side) );

			String string_id = "dummy" + i + "_f_" + source_point_name + "_2_" + destination_point_name;
			Id<Person> newId = Id.create(string_id, Person.class);

			/* Creating a person */
			Person person = population.getFactory().createPerson(newId);
			/* Creating a plan */
			Plan plan = population.getFactory().createPlan();

			int early_departure = 6;
			int late_departure  = 9;
			int activ_duration	= 3;
			/*
			 * Creating Activities
			 */
			List<Activity> activities = new ArrayList<Activity>();

			activities.add( createHome(source_point_coord, (rnd.nextInt(late_departure - early_departure + 1) + early_departure)*3600, 0 ) );
			activities.add( createWork(destination_point_coord, activ_duration*3600) );
			activities.add( createHome( source_point_coord, early_departure*3600, 0 ) );

			/* 
			 * Adding activities to the plan
			 */
			for (Activity activ : activities) {
				if ( activities.lastIndexOf(activ) != (activities.size() - 1) ) {
					plan.addActivity(activ);
					plan.addLeg(createDriveLeg());					
				}
			}

			/* we don't need a leg after this activity */
			plan.addActivity( activities.get( activities.size() - 1) );

			person.addPlan(plan);
			population.addPerson(person);
		}
		
	}

	/*
	 * Fills the map workplace_types.
	 */
	private void fillWorkplaceTypes() {
		/*
		 * Workplace Type file data structure.
		 * 
		 *  0	workplace_type		String		Tipo de Atractor
		 *  1	rate_of_persons		String		Porcentaje de empleados (por rama de actividad)
		 */		
		try {
			FileInputStream fis 	= new FileInputStream(workplaceTypesFile);
			InputStreamReader isr 	= new InputStreamReader(fis, Charset.forName("UTF-8"));
			BufferedReader buf_read = new BufferedReader(isr);

			buf_read.readLine(); /* skip header */

			String line;
			while ((line = buf_read.readLine()) != null) {
				String[] variables = line.split("\t");	/* separator = Tab */
				workplace_types.put( variables[0], Float.parseFloat(variables[1]) );

				// System.out.println(variables[0]);
				// System.out.println(Float.parseFloat(variables[1]));
			}

			buf_read.close();
			isr.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void generateTransactPersons() {
		/* random generator */
		Random rand = new Random();

		/* 
		 * the maximum and the minimum of hours 
		 * that a person might be with his/her transactions 
		 */
		int minTransactCount = 3;
		int maxTransactCount = 8;

		for ( int i = 0; i < (totalPersons*percentTransacting); i++ ) {
			/*
			 * Creating an ID for this person
			 */
			String string_id = "transact_" + i;
			Id<Person> newId = Id.create(string_id, Person.class);

			/* 
			 * Creating a person 
			 */
			Person person = population.getFactory().createPerson(newId);

			/* 
			 * Creating a plan 
			 */
			Plan plan = population.getFactory().createPlan();
			
			/* 
			 * Homeplace
			 * coordinates were already converted to cartesian points 
			 */
			Coord home_coords = getRandHomeplace();	

			/*
			 * Transaction start time (in seconds)
			 * 
			 * Will be initialized with the start time of the first 
			 * transaction activity
			 */
			int transactionsStartTime = 0;
			
			/*
			 * How many hours this person will be in her/his
			 * bussiness (in seconds).
			 */
			final int transactingCount = (rand.nextInt(maxTransactCount - minTransactCount + 1) + minTransactCount) * 3600;

			/*
			 * Transact hours accumulator.
			 * Accumulates the transacting hours of this person
			 */
			int transactingAcc = 0;		
			
			/*
			 * Permits take in count the time that the person
			 * needs to move to the next activity
			 */
			int legGap = 0;
			
			/*
			 * Activities
			 * 
			 * We'll create the list of transaction 
			 * activities, that this person must do
			 * 
			 * NOTE: Workplace should be undersended as Transacting Place
			 */
			List<Activity> transActivities = new ArrayList<Activity>();

			while ( transactingAcc < transactingCount ) {
				WorkPlace tp = getRandWorkplace();
				
				if ( transactingAcc == 0 ) { 	/* if it is the first activity... */
					transactionsStartTime = tp.morning_start_time; /* esta variable la necesito como referencia al fin del primer home */
					transactingAcc = tp.morning_start_time;
				}
				
				/* converting work coordinates into cartesian points */
				Coord transact_coords = new Coord(tp.longitude, tp.latitude);
				transact_coords = gtt.transform(transact_coords);

				transactingAcc+=tp.transaction_avg_time + legGap;
				legGap+=3600; /* 1 hour */

				transActivities.add( createWork(transact_coords, transactingAcc) );
			}
			
			/*
			 * Adding activities to the plan
			 */
			plan.addActivity( createHome(home_coords, transactionsStartTime , -3600) ); /* one hour of gap */
			plan.addLeg(createDriveLeg());

			for ( Activity transaction : transActivities ) {
				plan.addActivity(transaction);
				plan.addLeg(createDriveLeg());
			}
			
			plan.addActivity(createHome(home_coords, transactionsStartTime, -7200)); /* two hours of gap */
			
			person.addPlan(plan);

			population.addPerson(person);
		}
	}

	private void generateWorkers() {

		/* for each type of workplace */
		for (Map.Entry<String, Float> type : workplace_types.entrySet()) {
			// type.getKey() string  : type_name
			// type.getValue() float : rate_of_persons (who works in this... branch(?) )

			/* 
			 * The _type_ "correction" is used to generate a dummy population,
			 * which is used as a correction factor 
			 */
			if ( Objects.equals(type.getKey(), new String("correction")) ) {
				continue;
			}

			/* getting this _type_ workplaces */
			List<WorkPlace> workplaces = wpp.getWorkplaces(type.getKey());
			int maxWorkplace = workplaces.size();

			/* Random numbers generator */
			Random rand = new Random();

			/* 
			 * getting how many population will work there 
			 */
			float totalWorkers = totalPersons * percentWorkers * type.getValue();

			/* I know I'm comparing a float with an integer. I don't care */
			for ( int i = 0; i < totalWorkers; i++ ) {
				/* 
				 * Homeplace
				 * coordinates were already converted to cartesian points 
				 */
				Coord home_coords = getRandHomeplace();

				/*
				 * Pick a random workplace
				 */
				WorkPlace wp = workplaces.get( rand.nextInt(maxWorkplace) );

				/* converting work coordinates into cartesian points */
				Coord work_coords = new Coord(wp.longitude, wp.latitude);
				work_coords = gtt.transform(work_coords);

				String string_id = "w_from_" + i + "_to_" + wp.internal_name;
				Id<Person> newId = Id.create(string_id, Person.class);

				/* Creating a person */
				Person person = population.getFactory().createPerson(newId);
				/* Creating a plan */
				Plan plan = population.getFactory().createPlan();

				/*
				 * Creating Activities
				 */
				List<Activity> activities = new ArrayList<Activity>();

				/* one hour before work start time */
				activities.add( createHome(home_coords, wp.morning_start_time, -3600 ) );
				/**/
				activities.add( createWork(work_coords, wp.morning_end_time) );

				if ( wp.has_afternoon_period ) {
					/* por ahora hacemos que se vaya a la casa, pero, 
					 * si el tiempo estimado de viaje es mayor que la diferencia entre el horario
					 * de entrada a la tarde y salida a la mañana, debería quedarse dando vueltas o
					 * haciendo algo por ahí.
					 * Acá debería controlarse que la direfencia sea mayor al gap (por ahora lo 
					 * pongo) */
					activities.add( createHome(home_coords, wp.afternoon_start_time, -3600) );
					/**/
					activities.add( createWork(work_coords, wp.afternoon_end_time) );
				}
				/* last home activity. two hours before morning work start time */
				activities.add( createHome( home_coords, wp.morning_start_time, -7200 ) );
				
				/* 
				 * Adding activities to the plan
				 */
				for (Activity activ : activities) {
					if ( activities.lastIndexOf(activ) != (activities.size() - 1) ) {
						plan.addActivity(activ);
						plan.addLeg(createDriveLeg());					
					}
				}
				/* we don't need a leg after this activity */
				plan.addActivity( activities.get( activities.size() - 1) );

				person.addPlan(plan);

				population.addPerson(person);
			}  /* end for persons */
		} 	/* end for types */
	} 	/* end generateWorkers() */

	private Leg createDriveLeg() {
		/* Creating the route */
		Leg leg = population.getFactory().createLeg(TransportMode.car);
		return leg;
	}

	private Activity createWork(Coord location, int end_time) {
		Activity activity = population.getFactory().createActivityFromCoord("work", location);

		activity.setEndTime(end_time);

		return activity;
	}

	private Activity createHome(Coord location, int next_activity_start_time, int gap) {
		Activity activity = population.getFactory().createActivityFromCoord("home", location);

		activity.setEndTime(next_activity_start_time + gap);

		return activity;
	}

	/*
	 * Returns a workplace object from the list 'workplaces',
	 * between the range: 1 to maximum number of workplaces
	 * loaded from the file.
	 * 
	 * This randomness does not guarantee a good test environment
	 */
	private WorkPlace getRandWorkplace() {
		int maxWorkplace = workplaces.size();
		Random rand = new Random();
		return workplaces.get( rand.nextInt(maxWorkplace) );
	}

	/*
	 * Returns coordinates of a random homeplace,
	 * between the range: 1 to maximum number of homeplaces
	 * loaded from the file.
	 * 
	 * This randomness does not guarantee a good test environment
	 */
	private Coord getRandHomeplace() {
		int maxHomeplace = homeplaces.size();
		Random rand = new Random();
		// return homeplaces.get( rand.nextInt(maxHomeplace) + 1 );
		return homeplaces.get( rand.nextInt(maxHomeplace) );
	}

	private void fillHomeplaces() {
		/*
		 * Homeplaces file data structure.
		 * 
		 *  0	id_home		Integer		Home ID
		 *  1	home_name	String 		The homeplace name
		 *  2	longitude 	Double		THe longitude
		 *  3	latitude	Double 		The latitude
		 */		
		try {
			FileInputStream fis = new FileInputStream(homeEntriesFile);
			InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
			BufferedReader buf_read = new BufferedReader(isr);

			buf_read.readLine(); /* skip header */

			String line;
			while ((line = buf_read.readLine()) != null) {
				String[] variables = line.split("\t");	/* separator = Tab */

				Coord coords = new Coord(Double.parseDouble(variables[2]), Double.parseDouble(variables[3]));
				Coord coordinates = gtt.transform(coords);

				homeplaces.add(coordinates);
			}
			
			buf_read.close();
			isr.close();
			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

} /* end PopulationGenV2 */