package tpfsma.tucuroads;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WorkPlaceParser {
	private static final String sep = "\t"; /* TAB separator */
	private static final Charset charset = Charset.forName("UTF-8");
	private String filename = "";
	
	private List<WorkPlace> workplaces = new ArrayList<WorkPlace>();

	/**
	 *	Constructor
	 */
	WorkPlaceParser(String filename) {
		this.filename = filename;
	}

	public List<WorkPlace> getAllWorkPlaces() {
		return this.workplaces;
	}

	/*
	 * method getWorkplaces
	 * 
	 * Returns a list of workplaces of a given type.
	 * 
	 */
	public List<WorkPlace> getWorkplaces( String type ) {
		List<WorkPlace> result = new ArrayList<WorkPlace>();

		for ( WorkPlace singleWorkplace : this.workplaces ) {

			if ( Objects.equals(singleWorkplace.type, type) ) {
				result.add(singleWorkplace);
			}
		}

		return result;
	}
	
	public void fillWorkplacesFromFile() {
		/* workplaces list to return */
		List<WorkPlace> wp = new ArrayList<WorkPlace>();

		FileInputStream fis 	= null;
		InputStreamReader isr 	= null;
		BufferedReader buf_read = null;

		try {
			/* opening the file 'filename' */
			fis = new FileInputStream(filename);
			isr = new InputStreamReader(fis, charset);
			buf_read = new BufferedReader(isr);

			buf_read.readLine(); // skip first line

			String line;
			while ((line = buf_read.readLine()) != null) {
				WorkPlace workplace = new WorkPlace();

				String[] variables = line.split(sep);

				workplace.real_name 			= variables[0];
				workplace.internal_name 		= variables[1];
				workplace.type		 			= variables[2];
				workplace.longitude 			= Double.parseDouble(variables[3]);
				workplace.latitude 				= Double.parseDouble(variables[4]);
				workplace.morning_start_time 	= Integer.parseInt(variables[5]);
				workplace.morning_end_time 		= Integer.parseInt(variables[6]);
				workplace.transaction_avg_time 	= Integer.parseInt(variables[9]);

				if ( ! ( ( new String(variables[7]).equals("null") && new String(variables[8]).equals("null") ) ) ) {
					workplace.has_afternoon_period  = true;
					workplace.afternoon_start_time 	= Integer.parseInt(variables[7]);
					workplace.afternoon_end_time 	= Integer.parseInt(variables[8]);
				} else {
					workplace.has_afternoon_period  = false;
					workplace.afternoon_start_time 	= -1;
					workplace.afternoon_end_time 	= -1;
				}
				wp.add(workplace);
			}

			buf_read.close();
			isr.close();
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		} 

		this.workplaces = wp;
	}
}
