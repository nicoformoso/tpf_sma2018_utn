package tpfsma.tucuroads;

public class WorkPlace {
	/*
	 * Work Place data structure
	 * 
	 * Field					Comment
	 * -----					-------
	 * real_name				The real name of the workplace
	 * internal_name			An internal name to generate IDs
	 * type						The workplace _type_ of classification
	 * longitude				Longitude
	 * latitude					Latitude
	 * morning_start_time		Workplace morning start time (in seconds)
	 * morning_end_time			Workplace morning closing time (in seconds)
	 * afternoon_start_time		Workplace afternoon start time, if it has an afternoon work period (in seconds)
	 * afternoon_end_time		Workplace afternoon closing time, if it has an afternoon work period (in seconds)
	 * transaction_avg_time		Average duration of a transaction in such place (in seconds)
	 * 
	 * NOTE 1: If workplace has just one work period, afternoon_* must set to null
	 * NOTE 2: When type is integer, null is represented as zero (0)
	 */
	String real_name;
	String internal_name;
	String type;

	double longitude;
	double latitude;

	int morning_start_time;
	int morning_end_time;

	int afternoon_start_time;
	int afternoon_end_time;

	int transaction_avg_time;

	boolean has_afternoon_period;
}
