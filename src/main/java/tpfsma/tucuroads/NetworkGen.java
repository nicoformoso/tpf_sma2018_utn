package tpfsma.tucuroads;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.network.algorithms.NetworkCleaner;
import org.matsim.core.network.io.NetworkWriter;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.CoordinateTransformation;
import org.matsim.core.utils.geometry.transformations.TransformationFactory;
import org.matsim.core.utils.io.OsmNetworkReader;

public class NetworkGen {

	public static final String UTM20S = 
			"PROJCS[\"WGS 84 / UTM zone 20S\",\n" + 
			"    GEOGCS[\"WGS 84\",\n" + 
			"        DATUM[\"WGS_1984\",\n" + 
			"            SPHEROID[\"WGS 84\",6378137,298.257223563,\n" + 
			"                AUTHORITY[\"EPSG\",\"7030\"]],\n" + 
			"            AUTHORITY[\"EPSG\",\"6326\"]],\n" + 
			"        PRIMEM[\"Greenwich\",0,\n" + 
			"            AUTHORITY[\"EPSG\",\"8901\"]],\n" + 
			"        UNIT[\"degree\",0.01745329251994328,\n" + 
			"            AUTHORITY[\"EPSG\",\"9122\"]],\n" + 
			"        AUTHORITY[\"EPSG\",\"4326\"]],\n" + 
			"    UNIT[\"metre\",1,\n" + 
			"        AUTHORITY[\"EPSG\",\"9001\"]],\n" + 
			"    PROJECTION[\"Transverse_Mercator\"],\n" + 
			"    PARAMETER[\"latitude_of_origin\",0],\n" + 
			"    PARAMETER[\"central_meridian\",-63],\n" + 
			"    PARAMETER[\"scale_factor\",0.9996],\n" + 
			"    PARAMETER[\"false_easting\",500000],\n" + 
			"    PARAMETER[\"false_northing\",10000000],\n" + 
			"    AUTHORITY[\"EPSG\",\"32720\"],\n" + 
			"    AXIS[\"Easting\",EAST],\n" + 
			"    AXIS[\"Northing\",NORTH]]";
	
	public static void main(String[] args) {
		/*
		 * input osm file.
		 */
		String osm = "./input/tucuroads02.osm";

		/*
		 * The coordinate system to use. OpenStreetMap uses WGS84, but for MATSim, we need a projection where distances
		 * are (roughly) euclidean distances in meters.
		 * 
		 * No hay implementadas coordenadas para ninguna parte de América
		 * https://github.com/matsim-org/matsim/blob/master/matsim/src/main/java/org/matsim/core/utils/geometry/transformations/TransformationFactory.java
		 * 
		 */
		CoordinateTransformation ct = 
			 TransformationFactory.getCoordinateTransformation(TransformationFactory.WGS84, UTM20S);

		/*
		 * First, create a new Config and a new Scenario. One always has to do this when working with the MATSim 
		 * data containers.
		 * 
		 */
		Config config = ConfigUtils.createConfig();
		Scenario scenario = ScenarioUtils.createScenario(config);
		
		/*
		 * Pick the Network from the Scenario for convenience.
		 */
		Network network = scenario.getNetwork();

		OsmNetworkReader onr = new OsmNetworkReader(network,ct);
		onr.parse(osm);
		
		/*
		 * Clean the Network. Cleaning means removing disconnected components, so that afterwards there is a route from every link
		 * to every other link. This may not be the case in the initial network converted from OpenStreetMap.
		 */
		new NetworkCleaner().run(network);

		/*
		 * Write the Network to a MATSim network file.
		 */
		new NetworkWriter(network).write("./output/tucuroads/network02.xml");
		
		// Create an ESRI shape file from the MATSim network
		/*FeatureGeneratorBuilderImpl builder = new FeatureGeneratorBuilderImpl(net, UTM33N);
		builder.setWidthCoefficient(0.01);
		builder.setFeatureGeneratorPrototype(PolygonFeatureGenerator.class);
		builder.setWidthCalculatorPrototype(CapacityBasedWidthCalculator.class);
		new Links2ESRIShape(net,"./inputs/network.shp", builder).write();*/
	}

}
