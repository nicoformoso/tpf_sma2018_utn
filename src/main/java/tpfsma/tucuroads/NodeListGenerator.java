package tpfsma.tucuroads;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.Node;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.transformations.GeotoolsTransformation;
import org.matsim.core.utils.geometry.transformations.TransformationFactory;

/*
 * Class NodeListGenerator
 * 
 * Esta clase genera una lista de nodos en formato CSV, utilizada para encontrar 
 * los nodos conectados por los links (o tramos de calles) estudiados.
 * 
 * @subject Sistemas Multiagentes
 * @year    2018
 * 
 */
public class NodeListGenerator implements Runnable {
	/*
	 * Constants
	 */
	private static final String separator   = "\t";
	private static final String finalString = "\n";
	private static final String config_file = "./input/config.xml";
	private static final String output_file = "./output/tucuroads/nodelist.csv";
	private static final Charset charset = Charset.forName("UTF-8");
	/*
	 * Coordinates system
	 * 
	 * This Well Known Text (WKT) corresponds to the coordinates region where
	 * Tucumán is located in.
	 */
	private static final String UTM20S = "PROJCS[\"WGS 84 / UTM zone 20S\",\n" 
			+ "    GEOGCS[\"WGS 84\",\n"
			+ "        DATUM[\"WGS_1984\",\n" 
			+ "            SPHEROID[\"WGS 84\",6378137,298.257223563,\n"
			+ "                AUTHORITY[\"EPSG\",\"7030\"]],\n" 
			+ "            AUTHORITY[\"EPSG\",\"6326\"]],\n"
			+ "        PRIMEM[\"Greenwich\",0,\n" 
			+ "            AUTHORITY[\"EPSG\",\"8901\"]],\n"
			+ "        UNIT[\"degree\",0.01745329251994328,\n" 
			+ "            AUTHORITY[\"EPSG\",\"9122\"]],\n"
			+ "        AUTHORITY[\"EPSG\",\"4326\"]],\n" 
			+ "    UNIT[\"metre\",1,\n"
			+ "        AUTHORITY[\"EPSG\",\"9001\"]],\n" 
			+ "    PROJECTION[\"Transverse_Mercator\"],\n"
			+ "    PARAMETER[\"latitude_of_origin\",0],\n" 
			+ "    PARAMETER[\"central_meridian\",-63],\n"
			+ "    PARAMETER[\"scale_factor\",0.9996],\n" 
			+ "    PARAMETER[\"false_easting\",500000],\n"
			+ "    PARAMETER[\"false_northing\",10000000],\n" 
			+ "    AUTHORITY[\"EPSG\",\"32720\"],\n"
			+ "    AXIS[\"Easting\",EAST],\n" 
			+ "    AXIS[\"Northing\",NORTH]]";
	/*
	 * Variables
	 */
	private GeotoolsTransformation gtt = new GeotoolsTransformation(TransformationFactory.WGS84, UTM20S);
	private GeotoolsTransformation toLatLong = new GeotoolsTransformation(UTM20S, TransformationFactory.WGS84);

	private Network network;

	private Map<Id<Node>, Coord> cartessianCoordinates = new HashMap<Id<Node>, Coord>();

	public static void main(String[] args) {
		NodeListGenerator thatsme = new NodeListGenerator();

		thatsme.run();
	}

	public void run() {
		/*
		 * Loading Network file
		 */
		loadNetwork();

		/*
		 * Set cartessian coordinates variable map and Converting Coordinates into
		 * UTM20S System
		 */
		setCartesianCoordinates();

		/*
		 * Writing output file
		 */
		writeCoordinateFile();
		
	}

	private void loadNetwork() {
		Config configXML = ConfigUtils.loadConfig(config_file);
		this.network = ScenarioUtils.loadScenario(configXML).getNetwork();

		System.out.println("Node list size: " + this.network.getNodes().size() + ".\n");
	}

	private void setCartesianCoordinates() {
		for (Node node : this.network.getNodes().values()) {
			Id<Node> idNode = node.getId();
			Coord coordinates = toLatLong.transform(node.getCoord());
			cartessianCoordinates.put(idNode, coordinates);
		}
		System.out.println("Number of nodes: " + cartessianCoordinates.size() + "\n");
	}
	
	private void writeCoordinateFile() {

		FileOutputStream fos = null;

		try {
			fos = new FileOutputStream(output_file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		OutputStreamWriter osw = new OutputStreamWriter(fos, charset);
		BufferedWriter buff_writer = new BufferedWriter(osw);
		
		/* writing header */
		String header = 
				  "nodo_id"  + separator 
				+ "latitud"  + separator 
				+ "longitud" + separator 
				+ "x_value"  + separator 
				+ "y_value"  + finalString;
		try {
			buff_writer.write(header);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (Node node : this.network.getNodes().values() ) {
			String line;
			
			line = node.getId().toString() 						 + separator 
				+ cartessianCoordinates.get(node.getId()).getY() + separator
				+ cartessianCoordinates.get(node.getId()).getX() + separator
				+ node.getCoord().getX() 						 + separator 
				+ node.getCoord().getY() 						 + finalString;
			
			try {				
				buff_writer.write(line);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} /* end for */

		/* closing buffers */
		try {
			buff_writer.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	} /* end writeCoordinateFile */

} /* end class NodeListGenerator */
