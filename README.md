# Simulación de flujo de tráfico en la ciudad de San Miguel de Tucumán (MATSim)

* **Carrera:** Maestría y Especialización en Ingeniería en sistemas de Información
* **Materia:** Sistemas Multiagente
* **Cohorte:** 2017
* **Integrantes:**
	1. Nicolás Federico Formoso Requena
	2. Sergio Pérez Albert
	3. Juan Torres

---

## Descripción

El presente repositorio contiene los códigos en JAVA utilizados para la generación de los archivos XML necesarios para la simulación. Además, se incluyen dichos archivos XML.

---

### Estructura del repositorio

Archivo / Directorio	| Descripción
--------------------	| -----------
simulation\_input	| Directorio en el que se pueden encontrar los archivos XML para la ejecución de la simulación con MATSim.
simulation\_output 	| Directorio en el que (casualmente) pueden encontrarse archivos de corridas de simulaciones, necesarios para el análisis de las mismas.
src 			| Directorio en el que puede encontrarse el código fuente desarrollado para el presente trabajo de simulación.
pom.xml 		| Project Object Model - Archivo de información/configuración del proyecto Maven.
.gitignore 		| Lista de directorios y archivos que deben ser ignorados por el repositorio GIT.
README.md 		| Este archivo.

